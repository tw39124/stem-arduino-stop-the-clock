// File: seven_seg.cpp
// Author: Tom Woolfrey
//
// Driver for seven segment display (code)

#include "seven_seg.hpp"

#define DIGIT_TOGGLE_MILLIS 5U

// Map which convert a single-digit decimal value to port values for PORT D (Arduino pins 0-7)
// Bit 7 is the decimal point and is not used.
uint8_t DIGIT_TO_PORT_VALUE_MAP[10U] =
{
    0x3FU, // 0
    0x06U, // 1
    0x5BU, // 2
    0x4FU, // 3
    0x66U, // 4
    0x6DU, // 5
    0x7DU, // 6
    0x07U, // 7
    0x7FU, // 8
    0x6FU, // 9
};

constexpr int32_t t_SevenSeg::s_cai32DigitCathodePin[s_cu8DigitCount];

t_SevenSeg::t_SevenSeg() :
    m_u32LastDigitFlipTime(0U)
    , m_au8DigitPortValues { DIGIT_TO_PORT_VALUE_MAP[0U], DIGIT_TO_PORT_VALUE_MAP[0U] }
    , m_u8CurrentDigit(0U)
{
}

void t_SevenSeg::Enable()
{
    pinMode( 0U, OUTPUT );
    pinMode( 1U, OUTPUT );
    pinMode( 2U, OUTPUT );
    pinMode( 3U, OUTPUT );
    pinMode( 4U, OUTPUT );
    pinMode( 5U, OUTPUT );
    pinMode( 6U, OUTPUT );
    pinMode( s_cai32DigitCathodePin[0U], OUTPUT );
    pinMode( s_cai32DigitCathodePin[1U], OUTPUT );

    // Turn on digit 0
    WriteSevenSegDigitPort(m_au8DigitPortValues[0U]);
    digitalWrite( s_cai32DigitCathodePin[m_u8CurrentDigit], HIGH );

    m_u32LastDigitFlipTime = millis();
    m_bEnabled = true;
}

void t_SevenSeg::Process()
{
    if (m_bEnabled)
    {
        uint32_t u32CurrTime = millis();
        if( (u32CurrTime - m_u32LastDigitFlipTime) >= DIGIT_TOGGLE_MILLIS )
        {
            // Turn off current digit cathode (LOW = OFF)
            digitalWrite( s_cai32DigitCathodePin[m_u8CurrentDigit], LOW );
            
            m_u8CurrentDigit ^= 0x01U;   // Toggle between 0 and 1

            // Set port output and turn on new digit cathode (HIGH = ON)
            WriteSevenSegDigitPort(m_au8DigitPortValues[m_u8CurrentDigit]);
            digitalWrite( s_cai32DigitCathodePin[m_u8CurrentDigit], HIGH );

            m_u32LastDigitFlipTime = u32CurrTime;
        }
    }
}

void t_SevenSeg::SetValue(const uint8_t u8Value)
{
    m_au8DigitPortValues[0U] = DIGIT_TO_PORT_VALUE_MAP[(u8Value / 10U) % 10U];
    m_au8DigitPortValues[1U] = DIGIT_TO_PORT_VALUE_MAP[(u8Value % 10U)];
}

void t_SevenSeg::WriteSevenSegDigitPort(const uint8_t u8PortVal)
{
    PORTD = u8PortVal;
}
