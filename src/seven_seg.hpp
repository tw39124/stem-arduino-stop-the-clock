// File: seven_seg.hpp
// Author: Tom Woolfrey
//
// Driver for seven segment display (header)

#ifndef SEVEN_SEG_HPP
#define SEVEN_SEG_HPP

#include <stdint.h>
#include "Arduino.h"

// Driver for the 2-digit seven-segment display.
// The driver uses the persistence-of-vision effect to
// time-multiplex both digits onto the same set of MCU pins,
// using the common cathode of the digits to control which
// digit is currently on.
class t_SevenSeg
{
public:

    t_SevenSeg();

    // Periodic process (must be called at a regular rate
    // to guarantee correct 7-segment output)
    void Process();

    // Turns the seven-segment display on, with the last set
    // value
    void Enable();
    
    // Sets the value displayed on the 7-segment display. Only
    // the least-significant 2 decimal digits are used by this
    // function, everything else is ignored.
    void SetValue(const uint8_t u8Value);

private:

    static constexpr uint8_t s_cu8DigitCount = 2U;
    static constexpr int32_t s_cai32DigitCathodePin[s_cu8DigitCount] =
    {
        8,      // Tens digit
        9,      // Units digit
    };

    bool m_bEnabled;
    uint32_t m_u32LastDigitFlipTime;
    uint8_t m_au8DigitPortValues[s_cu8DigitCount];
    uint8_t m_u8CurrentDigit;

    void WriteSevenSegDigitPort(const uint8_t u8PortVal);
};

#endif