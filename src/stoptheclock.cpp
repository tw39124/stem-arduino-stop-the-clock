// File: main.ino#
// Author: Tom Woolfrey
//
// Main entry point for Arduino sketch.
// setup() called once on MCU initialisation.
// loop() called repeatedly until power is pulled.

#include <stdint.h>
#include "Arduino.h"
#include "seven_seg.hpp"

// IO pins in use
#define START_BUTTON_PIN  A0     // Input (with pull-up)
#define STOP_BUTTON_PIN A1       // Input (with pull-up)
#define RESET_BUTTON_PIN A2      // Input (with pull-up)
#define PASS_IND_OUTPUT_PIN A3   // Output
#define DEBUG_LED_PIN 13         // Output

// Flash periodically on pin 13 (which is connected to
// a debug LED) to indicate that the software is running.
uint32_t g_LastDebugLedPulseTime = 0U;

// Defusal state
enum class t_DefusalState
{
    Ready,      // Timer has been reset and is ready to run
    Running,    // Timer is running
    Stopped     // Timer has been stopped, and can be reset
};
t_DefusalState g_DefusalState = t_DefusalState::Ready;
uint8_t g_u8DefusalTimerValue = 0U;         // Timer value in tenths of a second
#define TIMER_VALUE_MAX 99U
#define TIMER_INCREMENT_MILLIS 50U         // Timer value increments every 50 ms
uint32_t g_u32LastTimeValIncrementTime = 0U;
#define TIMER_PASS_LOWER_LIMIT  73U
#define TIMER_PASS_UPPER_LIMIT  73U

t_SevenSeg g_SevenSegDriver;

// Button states
uint8_t g_u8StartButtonLatch = LOW;
uint8_t g_u8StopButtonLatch = LOW;
uint8_t g_u8ResetButtonLatch = LOW;

void reset_puzzle()
{
    g_DefusalState = t_DefusalState::Ready;
    g_u8DefusalTimerValue = 0U;
    g_SevenSegDriver.SetValue(g_u8DefusalTimerValue);
    digitalWrite(PASS_IND_OUTPUT_PIN, LOW);
}

void setup()
{
    pinMode(DEBUG_LED_PIN, OUTPUT);
    pinMode(START_BUTTON_PIN, INPUT_PULLUP);
    pinMode(STOP_BUTTON_PIN, INPUT_PULLUP);
    pinMode(RESET_BUTTON_PIN, INPUT_PULLUP);
    pinMode(PASS_IND_OUTPUT_PIN, OUTPUT);

    digitalWrite(DEBUG_LED_PIN, LOW);
    
    reset_puzzle();

    // Turn on 7-seg display
    g_SevenSegDriver.Enable();
}

void loop()
{
    const uint32_t cu32CurrTime = millis();
    if((cu32CurrTime - g_LastDebugLedPulseTime) >= 1000U)
    {
        // Time has elapsed, toggle the debug LED
        digitalWrite(DEBUG_LED_PIN, !digitalRead(DEBUG_LED_PIN));
        g_LastDebugLedPulseTime = cu32CurrTime;
    }

    // Allow the 7-seg driver to do its thing
    g_SevenSegDriver.Process();

    // Determine button states (trigger on rising edge only)
    const uint8_t cu8StartButtonState = digitalRead(START_BUTTON_PIN);
    const uint8_t cu8StopButtonState = digitalRead(STOP_BUTTON_PIN);
    const uint8_t cu8ResetButtonState = digitalRead(RESET_BUTTON_PIN);

    switch(g_DefusalState)
    {
        case t_DefusalState::Ready:
        {
			// Detect falling edge of start button
            if((cu8StartButtonState == LOW) && (g_u8StartButtonLatch == HIGH))
            {
                g_u32LastTimeValIncrementTime = cu32CurrTime;
                g_DefusalState = t_DefusalState::Running;
            }
            
            break;
        }
        case t_DefusalState::Running:
        {
			// Detect falling edge of stop button
            if((cu8StopButtonState == LOW) && (g_u8StopButtonLatch == HIGH))
            {
                g_DefusalState = t_DefusalState::Stopped;

                // Determine whether timer was stopped within the pass range
                if( (g_u8DefusalTimerValue >= TIMER_PASS_LOWER_LIMIT) &&
                    (g_u8DefusalTimerValue <= TIMER_PASS_UPPER_LIMIT) )
                {
                    // Pass!!
                    digitalWrite(PASS_IND_OUTPUT_PIN, HIGH);
                }
            }
            else if ((cu32CurrTime - g_u32LastTimeValIncrementTime) >= TIMER_INCREMENT_MILLIS)
            {
                // Increment the timer value and display on 7-seg
                ++g_u8DefusalTimerValue;
                g_SevenSegDriver.SetValue(g_u8DefusalTimerValue);

                // Go to stopped state if timer value has reached the maximum it can be
                if(g_u8DefusalTimerValue == TIMER_VALUE_MAX)
                {
                    g_DefusalState = t_DefusalState::Stopped;
                }

                g_u32LastTimeValIncrementTime = cu32CurrTime;
            }

            break;
        }
        case t_DefusalState::Stopped:
        {
			// Detect falling edge of reset button
            if((cu8ResetButtonState == LOW) && (g_u8ResetButtonLatch == HIGH))
            {
                // Reset to Ready state
                reset_puzzle();
            }
            
            break;
        }
        default:
        {
            // Something has screwed up if we have got here!
            // Reset to Ready state
            reset_puzzle();
            break;
        }
    }

    // Update button latches
    g_u8StartButtonLatch = cu8StartButtonState;
    g_u8StopButtonLatch = cu8StopButtonState;
    g_u8ResetButtonLatch = cu8ResetButtonState;
}
