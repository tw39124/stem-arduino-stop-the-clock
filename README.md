# stem-arduino-stoptheclock

Arduino Uno project to control the "Stop The Clock" puzzle. The project uses a GNU Makefile with avr-gcc to build for the atmega328p.

## Hardware

The seven-segment display unit is the F5101AH: two digits are supported by wiring the 7 segments A-G from the Arduino to both F5101AH devices, and the common cathode pins to each digit separately. The two 7-segment displays are multiplexed and the software alternates drive current between the two digits at a high speed to give the illusion that both digits are on at once.

The F5101AH pin layout is as follows (looking top-down):

![](./f5101ah.png)

The following Arduino Uno pins are used. The decimal point segment is not currently used by the software and can be left unconnected.

| Arduino Uno pin | Purpose                                                                  |
| --------------- | ------------------------------------------------------------------------ |
| 0               | Segment A (F5101AH pin 7)                                                |
| 1               | Segment B (F5101AH pin 6)                                                |
| 2               | Segment C (F5101AH pin 4)                                                |
| 3               | Segment D (F5101AH pin 2)                                                |
| 4               | Segment E (F5101AH pin 1)                                                |
| 5               | Segment F (F5101AH pin 9)                                                |
| 6               | Segment G (F5101AH pin 10)                                               |
| 7               | Decimal point (F5101AH pin 5) [unused in software]                       |
| 8               | Digit 1 cathode [See note 1]                                             |
| 9               | Digit 2 cathode [See note 1]                                             |
| A0              | "Start" button momentary input (Vcc=unpressed, GND=pressed) [see note 2] |
| A1              | "Stop" button input (Vcc=unpressed, GND=pressed)            [see note 2] |
| A2              | "Reset" button input (Vcc=unpressed, GND=pressed)           [see note 2] |
| A3              | "Success" indicator output (Vcc=ON, GND=OFF)                             |


Note 1: To avoid over-currenting the MCU pins if all segments are turned on at the same time, pins 8 and 9 of the MCU should drive the base of an NPN transistor (via a 22K resistor). The NPN collector-emitter circuit should then link the F5101AH cathode pin (3 or 8) to GND via a 220Ohm resistor to limit the LED forward current. The ATmega328p pins have a current sink limit of 200mA which might be exceeded if it is used directly as the common cathode and all segments are turned on at the same time.

Note 2: The other side of the momentary push button should be connected to GND. Arduino software configures 20K internal pull-up.

Note: Arduino pin 13 is used to flash an on-board debug LED (to indicate that the software is running) and should not be connected off-board.

Below is a rudimentary diagram showing how this should be connected up. Arduino power & programming not shown.

![](./circuit1.png)

## Software

### Files

- stoptheclock.cpp: the main code file, controlling the puzzle's state machine
- seven_seg.cpp/hpp: driver for the two-digit 7-segment display

### Setup

- Install Arduino toolchain (sudo apt-get install arduino) to get
  avr-gcc and the required Arduino headers/libs
- Install avrdude (sudo apt-get install avrdude) which is the Arduino programming tool

### Building 

Simply run "make" inside the project directory. You may need to override the following Makefile vars:

- CC, CXX, AS, LD and OBJCOPY: must point to your install of avr-gcc
- ARDUINO_SRC_DIR: Arduino code files (default: /usr/share/arduino/hardware/arduino/cores/arduino)
- ARDUINO_INCLUDES: Arduino header files (/usr/share/arduino/hardware/arduino/cores/arduino and /usr/share/arduino/hardware/arduino/variants/standard)

The output will be generated in the "build" directory. The .hex file should be supported by the Arduino USB bootloader.

### Programming

Run "make upload AVR_COM=XXXXX", where XXXX is the COM port connected to the Arduino. This will upload the .hex file generated to the Arduino. You may need to override the following Makefile vars:

- AVRDUDE: must point to your install of avrdude
